package com.msgreader.android.msgreader.db;

/**
 * Created by o.kusakina on 13.12.2015.
 */
public class Contract {

    private Contract () {
    }

    public class NewsColumns extends BaseColumns {
        public static final String TABLE_NAME = "news";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_CONTENT = "content";
        public static final String COLUMN_TIMESTAMP = "timestamp";
    }

    public class BaseColumns {
        public static final String _ID = "_id";
    }
}
