package com.msgreader.android.msgreader.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.msgreader.android.msgreader.App;

import java.util.List;

/**
 * Created by o.kusakina on 13.12.2015.
 */
public class DatabaseManager {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "news.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_NEWS = "CREATE TABLE " + Contract.NewsColumns.TABLE_NAME +
            "(" + Contract.NewsColumns._ID + " INTEGER PRIMARY KEY," +
            Contract.NewsColumns.COLUMN_TITLE + TEXT_TYPE + COMMA_SEP +
            Contract.NewsColumns.COLUMN_CONTENT + TEXT_TYPE + COMMA_SEP +
            Contract.NewsColumns.COLUMN_TIMESTAMP + INTEGER_TYPE + ")";
    private static final String SQL_DELETE_NEWS = "DROP TABLE IF EXISTS " + Contract.NewsColumns.TABLE_NAME;
    private static final DatabaseManager MANAGER = new DatabaseManager();

    private SQLiteOpenHelper mHelper;
    private SQLiteDatabase mWritableDatabase;
    private SQLiteDatabase mReadableDatabase;

    private DatabaseManager () {
        mHelper = new DatabaseHelper(App.getInstance());
    }

    public static DatabaseManager getInstance () {
        return MANAGER;
    }

    public void open () {
        mWritableDatabase = mHelper.getWritableDatabase();
        mReadableDatabase = mHelper.getReadableDatabase();

    }

    public void close () {
        if (mReadableDatabase != null) {
            mReadableDatabase.close();
            mReadableDatabase = null;
        }
        if (mWritableDatabase != null) {
            mWritableDatabase.close();
            mWritableDatabase = null;
        }
    }

    public void insertNews (List<ContentValues> valuesList) {
        for (ContentValues values : valuesList) {
            mWritableDatabase.insert(Contract.NewsColumns.TABLE_NAME, null, values);
        }
    }

    public Cursor getAllNews () {
        final String[] projection = {Contract.NewsColumns._ID, Contract.NewsColumns.COLUMN_TITLE, Contract.NewsColumns.COLUMN_CONTENT, Contract.NewsColumns.COLUMN_TIMESTAMP};
        Cursor cursor = mReadableDatabase.query(Contract.NewsColumns.TABLE_NAME, projection, null, null, null, null, Contract.NewsColumns.COLUMN_TIMESTAMP + " DESC");
        return cursor;
    }

    public int clearNews () {
        final int deletedCount = mWritableDatabase.delete(Contract.NewsColumns.TABLE_NAME, "1", null);
        return deletedCount;
    }

    private class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper (Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public DatabaseHelper (Context context, DatabaseErrorHandler errorHandler) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION, errorHandler);
        }

        @Override
        public void onCreate (SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_NEWS);
        }

        @Override
        public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_NEWS);
            onCreate(db);
        }
    }
}
