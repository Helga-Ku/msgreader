package com.msgreader.android.msgreader.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msgreader.android.msgreader.R;
import com.msgreader.android.msgreader.adapters.CursorAdapter;
import com.msgreader.android.msgreader.adapters.NewsAdapter;
import com.msgreader.android.msgreader.loaders.AssetsNewsLoader;

public class ItemsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int NEWS_LOADER = 1;
    private OnListFragmentInteractionListener mListener;
    private CursorAdapter mAdapter;

    public ItemsFragment () {
    }

    public static ItemsFragment newInstance () {
        ItemsFragment fragment = new ItemsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(mAdapter);
        }
        getActivity().setTitle(R.string.news_title);
        return view;
    }


    @Override
    public void onAttach (Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
        mAdapter = new NewsAdapter(context, mListener);
    }

    @Override
    public void onResume () {
        super.onResume();
        getLoaderManager().initLoader(NEWS_LOADER, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader (int id, Bundle args) {
        return new AssetsNewsLoader(getActivity());
    }

    @Override
    public void onLoadFinished (Loader<Cursor> loader, Cursor data) {
        mAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset (Loader<Cursor> loader) {

    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction (String title, String content, long timestamp);
    }
}
