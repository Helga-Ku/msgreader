package com.msgreader.android.msgreader.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import com.msgreader.android.msgreader.db.Contract;
import com.msgreader.android.msgreader.db.DatabaseManager;
import com.msgreader.android.msgreader.models.NewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by o.kusakina on 13.12.2015.
 */
public abstract class NewsLoaderAbs extends AsyncTaskLoader<Cursor> {
    private DatabaseManager mManager = DatabaseManager.getInstance();

    public NewsLoaderAbs (Context context) {
        super(context);
        forceLoad();
    }

    @Override
    public Cursor loadInBackground () {
        List<ContentValues> valuesList = new ArrayList<>();
        valuesList.addAll(convertModels(loadNews()));
        mManager.clearNews();
        mManager.insertNews(valuesList);
        return mManager.getAllNews();
    }

    protected abstract List<NewsModel> loadNews();

    protected static List<ContentValues> convertModels(List<NewsModel> models){
        final List<ContentValues> list = new ArrayList<>();
        for (NewsModel model : models) {
            list.add(convertModel(model));
        }
        return list;
    }

    protected static ContentValues convertModel(NewsModel model) {
        ContentValues values = new ContentValues();
        values.put(Contract.NewsColumns.COLUMN_TITLE, model.getSubject());
        values.put(Contract.NewsColumns.COLUMN_CONTENT, model.getBody());
        values.put(Contract.NewsColumns.COLUMN_TIMESTAMP, model.getTimestamp());
        return values;
    }
}
