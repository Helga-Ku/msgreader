package com.msgreader.android.msgreader.loaders;

import android.content.Context;

import com.msgreader.android.msgreader.models.NewsModel;
import com.msgreader.android.msgreader.utils.NewsParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by o.kusakina on 14.12.2015.
 */
public class AssetsNewsLoader extends NewsLoaderAbs {
    public AssetsNewsLoader (Context context) {
        super(context);
    }

    @Override
    protected List<NewsModel> loadNews () {
        List<NewsModel> valuesList = new ArrayList<>();
        try {
            InputStream is = getContext().getAssets().open("15_10_2015.msg");
            valuesList.addAll(NewsParser.getInstance().parse(is));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return valuesList;
    }
}
