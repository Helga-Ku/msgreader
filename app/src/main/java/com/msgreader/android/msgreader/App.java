package com.msgreader.android.msgreader;

import android.app.Application;

import com.msgreader.android.msgreader.utils.PreferenceManager;

/**
 * Created by o.kusakina on 13.12.2015.
 */
public class App extends Application {
    private static App sInstance;

    @Override
    public void onCreate () {
        super.onCreate();
        sInstance = this;
        PreferenceManager.getInstance().init(this);
    }

    public static App getInstance () {
        return sInstance;
    }
}
