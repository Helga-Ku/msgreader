package com.msgreader.android.msgreader.models;

/**
 * Created by o.kusakina on 14.12.2015.
 */
public class NewsModel {
    private String mFrom;
    private String mSubject;
    private String mBody;
    private long mTimestamp;

    public String getFrom () {
        return mFrom;
    }

    public void setFrom (String from) {
        this.mFrom = from;
    }

    public String getSubject () {
        return mSubject;
    }

    public void setSubject (String subject) {
        this.mSubject = subject;
    }

    public String getBody () {
        return mBody;
    }

    public void setBody (String body) {
        this.mBody = body;
    }

    public long getTimestamp () {
        return mTimestamp;
    }

    public void setTimestamp (long timestamp) {
        this.mTimestamp = timestamp;
    }
}
