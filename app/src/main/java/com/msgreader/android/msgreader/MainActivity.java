package com.msgreader.android.msgreader;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.msgreader.android.msgreader.db.DatabaseManager;
import com.msgreader.android.msgreader.fragments.ItemsFragment;
import com.msgreader.android.msgreader.fragments.ItemFragment;

public class MainActivity extends AppCompatActivity implements ItemsFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseManager.getInstance().open();

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged () {
                getSupportActionBar().setDisplayHomeAsUpEnabled(getSupportFragmentManager().getBackStackEntryCount() > 0);
            }
        });
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, ItemsFragment.newInstance())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openItem(String title, String content, long timestamp) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, ItemFragment.newInstance(title, content, timestamp))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onListFragmentInteraction (String title, String content, long timestamp) {
        openItem(title, content, timestamp);
    }

    @Override
    protected void onDestroy () {
        DatabaseManager.getInstance().close();
        super.onDestroy();
    }
}
