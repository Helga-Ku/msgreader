package com.msgreader.android.msgreader.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by o.kusakina on 14.12.2015.
 */
public class PreferenceManager {
    private static final PreferenceManager INSTANCE = new PreferenceManager();
    private static final String PREFS = PreferenceManager.class.getPackage().getName() + "_preferences";
    private static final String PREF_LAST_SYNC_TIME = "pref_last_sync_time";

    private Context mContext;
    private SharedPreferences mPrefs;

    private PreferenceManager(){}

    public static PreferenceManager getInstance () {
        return INSTANCE;
    }

    public void init (Context context) {
        mContext = context;
        mPrefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    public long getLastSyncTime () {
        if (mPrefs != null) {
            return mPrefs.getLong(PREF_LAST_SYNC_TIME, -1);
        }
        return -1;
    }

    public void setLastSyncTime (long lastSyncTime) {
        if (mPrefs != null) {
            mPrefs.edit().putLong(PREF_LAST_SYNC_TIME, lastSyncTime).commit();
        }
    }
}
