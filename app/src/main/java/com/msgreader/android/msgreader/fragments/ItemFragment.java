package com.msgreader.android.msgreader.fragments;

import android.content.ClipDescription;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.msgreader.android.msgreader.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ItemFragment extends Fragment {
    private static final String ARG_TITLE = "title";
    private static final String ARG_CONTENT = "content";
    private static final String ARG_TIMESTAMP = "timestamp";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy");

    private WebView mWebView;
    private String mTitle;
    private String mContent;
    private long mTimestamp;

    public ItemFragment () {
    }

    public static ItemFragment newInstance (String title, String content, long timestamp) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_CONTENT, content);
        args.putLong(ARG_TIMESTAMP, timestamp);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString(ARG_TITLE);
            mContent = getArguments().getString(ARG_CONTENT);
            mTimestamp = getArguments().getLong(ARG_TIMESTAMP);
        }
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        mWebView = (WebView) inflater.inflate(R.layout.fragment_item, container, false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().getLoadWithOverviewMode();
        mWebView.loadDataWithBaseURL(null, mContent, ClipDescription.MIMETYPE_TEXT_HTML, "utf-8", null);
        getActivity().setTitle(mTitle);
        return mWebView;
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach () {
        super.onDetach();
    }
}
