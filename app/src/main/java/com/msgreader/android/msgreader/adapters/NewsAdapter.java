package com.msgreader.android.msgreader.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.msgreader.android.msgreader.R;
import com.msgreader.android.msgreader.db.Contract;
import com.msgreader.android.msgreader.fragments.ItemsFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by o.kusakina on 13.12.2015.
 */
public class NewsAdapter extends CursorAdapter<NewsAdapter.ItemHolder> {
    private ItemsFragment.OnListFragmentInteractionListener mListener;

    public NewsAdapter (Context context, ItemsFragment.OnListFragmentInteractionListener listener) {
        super(context);
        mListener = listener;
    }

    @Override
    public void onBindViewHolder (final ItemHolder viewHolder, final Cursor cursor) {
        viewHolder.bind(cursor, mListener);
    }

    @Override
    public ItemHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.li_item, parent, false));
    }

    protected static class ItemHolder extends RecyclerView.ViewHolder {
        private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy");
        private View mView;
        private TextView mTitleView;
        private TextView mDetailsView;
        private TextView mTimestampView;
        private String mTitle;
        private String mContent;
        private long mTimestamp;

        public ItemHolder (View itemView) {
            super(itemView);
            mView = itemView;
            mTitleView = (TextView) itemView.findViewById(R.id.title);
            mDetailsView = (TextView) itemView.findViewById(R.id.details);
            mTimestampView = (TextView) itemView.findViewById(R.id.timestamp);
        }

        public void bind (Cursor cursor, final ItemsFragment.OnListFragmentInteractionListener listener) {
            mTitle = cursor.getString(cursor.getColumnIndex(Contract.NewsColumns.COLUMN_TITLE));
            mContent = cursor.getString(cursor.getColumnIndex(Contract.NewsColumns.COLUMN_CONTENT));
            mTimestamp = cursor.getLong(cursor.getColumnIndex(Contract.NewsColumns.COLUMN_TIMESTAMP));
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick (View v) {
                    if (listener != null) {
                        listener.onListFragmentInteraction(mTitle, mContent, mTimestamp);
                    }
                }
            });
            mTitleView.setText(Html.fromHtml(mTitle));
            mDetailsView.setText(Html.fromHtml(mContent));
            mTimestampView.setText(DATE_FORMAT.format(new Date(mTimestamp)));
        }
    }
}
