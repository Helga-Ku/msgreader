package com.msgreader.android.msgreader.utils;

import com.msgreader.android.msgreader.models.NewsModel;

import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by o.kusakina on 13.12.2015.
 */
public class NewsParser {

    private static final NewsParser INSTANCE = new NewsParser();

    private NewsParser () {
    }

    public static NewsParser getInstance () {
        return INSTANCE;
    }

    public List<NewsModel> parse (InputStream is) {
        final List<NewsModel> models = new ArrayList<>();
        try {
            final MAPIMessage msg = new MAPIMessage(is);
            final NewsModel model = new NewsModel();
            try {
                model.setBody(msg.getTextBody());
            } catch (ChunkNotFoundException e) {
                e.printStackTrace();
                model.setBody("");
            }
            try {
                model.setTimestamp(msg.getMessageDate().getTimeInMillis());
            } catch (ChunkNotFoundException e) {
                e.printStackTrace();
                model.setTimestamp(System.currentTimeMillis());
            }
            try {
                model.setSubject(msg.getSubject());
            } catch (ChunkNotFoundException e) {
                e.printStackTrace();
                model.setSubject("");
            }
            try {
                model.setFrom(msg.getDisplayFrom());
            } catch (ChunkNotFoundException e) {
                e.printStackTrace();
                model.setFrom("");
            }
            models.add(model);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return models;
    }
}
